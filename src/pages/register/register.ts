import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {LoginPage} from "../login/login";
import {HomePage} from "../home/home";
import {LoginService} from '../../services/login-service';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  busNo:any;
  route:any;
  capacity:any;
  email:any;
  password:any;
  role:String = "1"
  constructor(public nav: NavController,public loginService:LoginService) {
  }

  // register and go to home page
  request:any={};
  register() {
    console.log(this.email)
    console.log(this.route);
    this.request.email = this.email;
    this.request.password = this.password;
    this.request.busNo = this.busNo;
    this.request.capacity = this.capacity;
    this.request.route = this.route;
    this.request.role = this.role;
    console.log(this.request);
    this.loginService.registerUSer(this.request)
    .subscribe((data)=>{
      console.log(data);
      window.localStorage['user']= JSON.stringify(data);
      this.nav.setRoot(HomePage);
    },(error)=>{
      console.log(error);
    })
  }

  // go to login page
  login() {
    this.nav.setRoot(LoginPage);
  }
}
