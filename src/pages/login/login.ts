import { Component } from "@angular/core";
import { NavController, AlertController, ToastController, MenuController } from "ionic-angular";
import { MapPage } from "../map/map";
import { RegisterPage } from "../register/register";
import { LoginService } from '../../services/login-service';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  user: any = {
    email: '',
    password: ''
  };
  constructor(public nav: NavController, public forgotCtrl: AlertController, public menu: MenuController,
    public toastCtrl: ToastController, public loginService: LoginService) {
    this.menu.swipeEnable(false);
  }

  // go to register page
  register() {
    this.nav.setRoot(RegisterPage);
  }

  // login and go to home page
  login() {
    // this.nav.setRoot(MapPage);
      this.loginService.loginService(this.user)
        .subscribe((data) => {
          let toast = this.toastCtrl.create({
            message: 'Welcome to the iBus',
            duration: 2000,
            position: 'bottom'
          });

          toast.present();
          window.localStorage['data'] = JSON.stringify(data);
          this.nav.setRoot(MapPage);
        }, (error) => {
          const alert = this.forgotCtrl.create({
            title: 'Error!',
            subTitle: error.msg,
            buttons: ['OK']
          });
          alert.present();
        })

  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
