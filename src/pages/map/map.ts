import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationProvider } from '../../services/location-service';
import { AlertController } from 'ionic-angular';
import { mapConfig } from '../../config/mapConfig';
import { LocationSocketProvider } from '../../providers/location-socket/location-socket';
/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild('map') mapElement: ElementRef;;
  isStarted: boolean = false;
  currentLocation: any;
  map: any;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  markers: any = [];
  busData: any = JSON.parse(window.localStorage['data']);
  watch: any;
  locations: any;

  constructor(private alertCtrl: AlertController, public navCtrl: NavController, private geolocation: Geolocation,
    public locationProvider: LocationProvider, private locationSocket: LocationSocketProvider) {
    console.log(this.busData);
    this.validateJourney();
  }

  /**
   * check weather the journey is already started or not
   */
  validateJourney() {
    if (window.localStorage.getItem("journey") != null && window.localStorage.getItem("journey") != undefined) {
      // this.initMap();
      this.isStarted = true;
    } else {
      this.isStarted = false;
    }
  }

  /**
   * call the checkMapNative element method 
   */
  ionViewDidLoad() {
    this.checkMapNativeElemente();
  }

  /**
   * check weather the map element is loaded or not
   */
  checkMapNativeElemente() {
    let mapEle = this.mapElement.nativeElement;
    if (!mapEle) {
      console.error('Unable to initialize map, no map element with #map view reference.');
      return;
    } else {
      this.loadMap(mapEle)
    }
  }

  sendMessage() {
    this.locationSocket.sendMessage("call");
  }

  // ngOnInit() {
  //   this.locations = this.locationSocket.getMessages().subscribe(message => {
  //     this.messages.push(message);
  //   })
  // }

  ngOnDestroy() {
    if (this.locations != null && this.locations != undefined)
      this.locations.unsubscribe();
  }
  /**
   * load map according to the Drivers current location
   * @param mapEle 
   */
  loadMap(mapEle) {
    
    let scope = this;
    this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 10000, enableHighAccuracy: true }).then((resp) => {
      scope.currentLocation = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      scope.map = new google.maps.Map(mapEle, {
        zoom: 15,
        center: scope.currentLocation,
        scrollwheel: false,
        styles: mapConfig.styles,
        panControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        fullScreenControl: false
      });
    }).catch((error) => {
      console.log(error);
      this.showAlert(error);
    }).then(() => {
      this.calculateAndDisplayRoute(this.directionsService, this.directionsDisplay);
    })
  }

  /**
   * drow the route according to the bus driver using way points
   */
  calculateAndDisplayRoute(directionsService, directionsDisplay) {
    let scope = this;
    // var waypts = [{ location: new google.maps.LatLng(6.924532, 79.978428) },
    // { location: new google.maps.LatLng(6.903943, 79.955009) },
    // { location: new google.maps.LatLng(6.907623, 79.929085) },
    // { location: new google.maps.LatLng(6.902906, 79.908347) },
    // { location: new google.maps.LatLng(6.910562, 79.887805) },
    // { location: new google.maps.LatLng(6.902906, 79.908347) }];
    directionsService.route({
      origin: "Kaduwela",
      destination: "Kollupitiya",
      travelMode: 'DRIVING',
      // waypoints: waypts
    }, function (response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
        directionsDisplay.setOptions({ suppressMarkers: true });
        directionsDisplay.setMap(scope.map)
        scope.viewBuses();
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

  /**
   * get active tracker locations from the database and view on the map
   */
  viewBuses() {
    let scope = this;
    this.locations = this.locationSocket.getMessages().subscribe(data => {
      scope.deleteMarkers();
      console.log(data)
      var t = JSON.parse(JSON.stringify(data))
      t.data.forEach(element => {
        let updatelocation = new google.maps.LatLng(element.latitude, element.longitude);
        var image = "assets/img/busmarker.png"
        this.addMarker(updatelocation, image);
      })
      this.setMapOnAll(this.map);
    });
    this.sendMessage();
  }

  addMarker(location, image) {
    var icon = {
      url: image, // url
      scaledSize: new google.maps.Size(30, 30), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    };
    console.log(location);

    // animation: google.maps.Animation.BOUNCE,
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: icon
    });
    this.markers.push(marker);
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }

  /**
   * set single marker using input object
   * @param location
   */
  setSingleMarker(location) {
    console.log(location);
    // new google.maps.Marker({
    //   position: location,
    //   map: this.map
    // });
    this.map.setCenter(location);
    this.map.setZoom(15);
  }

  /**
   * start geolation watch and get the cordinates when cordinate changes
   */
  startJourney() {
    let scope = this;
    this.watch = this.geolocation.watchPosition().subscribe((data) => {
      scope.currentLocation = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
      scope.setSingleMarker(scope.currentLocation);
      this.sendLocation({ longitude: data.coords.longitude, latitude: data.coords.latitude, bus_id: scope.busData.data.id })
    }, (error) => {
      console.log(error);
    });
  }

  /**
   * check weather the journey already started or not if startesd update the tracker else create a new journey
   * @param obj 
   */
  sendLocation(obj) {
    let scope = this;
    if (window.localStorage['tracker'] == null || window.localStorage['tracker'] == undefined) {
      this.locationProvider.startTracking({ longitude: obj.longitude, latitude: obj.latitude, bus: this.busData.data, status: "Active" })
        .subscribe((data) => {
          var track = JSON.parse(JSON.stringify(data));
          window.localStorage['tracker'] = JSON.stringify(data);
          this.locationProvider.createJourney({ startTime: Date.now(), bus: this.busData.data, tracker: track.data })
            .subscribe((journey) => {
              
             scope.sendMessage();
              window.localStorage['journey'] = JSON.stringify(journey);
            }, (err) => {
              console.log(err);
            })
        }, (error) => {
          console.log(error)
        })
    } else {
      var tracker = JSON.parse(window.localStorage['tracker']);
      this.locationProvider.updateTracking({ tracking_id: tracker.data._id, longitude: obj.longitude, latitude: obj.latitude, bus: this.busData.data, status: "Active" })
        .subscribe((data) => {
          window.localStorage['tracker'] = JSON.stringify(data);
        }, (error) => {
          console.log(error);
        })
    }
    this.locationProvider.sendLocation(obj)
      .subscribe((data) => {
        console.log(data)
      })
  }

  /**
   * finish journey and deactivate the tracker from the server
   */
  finishJourney() {
    let scope = this;
    if (this.watch != null && this.watch != undefined) {
      this.watch.unsubscribe();
    }
    // if (this.locations != null || this.locations != undefined) {
    //   this.ngOnDestroy();
    // }
    this.isStarted = false;
    var tracker = JSON.parse(window.localStorage['tracker'])
    this.locationProvider.updateTracking({ tracking_id: tracker.data._id, longitude: tracker.data.longitude, latitude: tracker.data.latitude, status: "Deactive" })
      .subscribe((tracking) => {
        var journey = JSON.parse(window.localStorage['journey'])
        this.locationProvider.endJourney({ journey_id: journey.data._id, endTime: Date.now() })
          .subscribe((journey) => {
            window.localStorage.removeItem('journey');
            window.localStorage.removeItem('tracker');
            
            scope.sendMessage();
            this.checkMapNativeElemente();
            console.log(journey)
          }, (error) => {
            console.log(error);
          });
      }, (error) => {
        console.log(error)
      });
  }

  /**
   * show confirm journey alert
   */
  startJourneyAlert() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Journey',
      message: 'Do you want to start journey?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.startJourney();
            this.isStarted = true;

          }
        }
      ]
    });
    alert.present();
  }

  /**
   * show finish journey alert
   */
  finishJourneyAlert() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Journey',
      message: 'Do you want to finish journey?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.finishJourney();
          }
        }
      ]
    });
    alert.present();
  }

  showAlert(error) {
    const alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: error,
      buttons: ['OK']
    });
    alert.present();
  }
}
