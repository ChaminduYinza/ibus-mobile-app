import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { LoginPage } from "../login/login";


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  profile = {
    name: "Chamindu Thiranjaya",
    language: "en",
    email: "chamindu.cta@gmail.com",
    contact:"0717380347",
    password:"1231231213",
    newPassword:"1231231213",
    confirmPassword:"1231231213"
  }

  constructor(public nav: NavController) {
  }

  // logout
  logout() {
    this.nav.setRoot(LoginPage);
  }
}
