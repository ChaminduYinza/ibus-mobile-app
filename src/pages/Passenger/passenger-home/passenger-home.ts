import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationProvider } from '../../../services/location-service';
import { mapConfig } from '../../../config/mapConfig';
import { LocationSocketProvider } from '../../../providers/location-socket/location-socket';
/**
 * Generated class for the PassengerHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-passenger-home',
  templateUrl: 'passenger-home.html',
})
export class PassengerHomePage {
  @ViewChild('map') mapElement: ElementRef;;
  isStarted: boolean = false;
  currentLocation: any;
  time = "00:00:00";
  map: any;
  route: any;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  markers: any = [];
  busData: any = JSON.parse(window.localStorage['data']);
  watch: any;
  locations: any;

  constructor(public navCtrl: NavController, private geolocation: Geolocation,
    public locationProvider: LocationProvider, private locationSocket: LocationSocketProvider) {
    console.log(this.busData);
    this.validateJourney();
  }

  /**
   * check weather the journey is already started or not
   */
  validateJourney() {
    if (window.localStorage.getItem("journey") != null && window.localStorage.getItem("journey") != undefined) {
      // this.initMap();
      this.isStarted = true;
    } else {
      this.isStarted = false;
    }
  }

  /**
   * call the checkMapNative element method 
   */
  ionViewDidLoad() {
    this.checkMapNativeElemente();
  }

  /**
   * check weather the map element is loaded or not
   */
  checkMapNativeElemente() {
    let mapEle = this.mapElement.nativeElement;
    if (!mapEle) {
      console.error('Unable to initialize map, no map element with #map view reference.');
      return;
    } else {
      this.loadMap(mapEle)
    }
  }

  sendMessage() {
    console.log("message")
    this.locationSocket.sendMessage("call");
  }

  // ngOnInit() {
  //   this.locations = this.locationSocket.getMessages().subscribe(message => {
  //     this.messages.push(message);
  //   })
  // }

  ngOnDestroy() {
    if (this.locations != null && this.locations != undefined) {
      this.locations.unsubscribe();
    }
  }
  /**
   * load map according to the Drivers current location
   * @param mapEle 
   */
  loadMap(mapEle) {
    let scope = this;
    this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 10000, enableHighAccuracy: true }).then((resp) => {
      scope.currentLocation = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      scope.map = new google.maps.Map(mapEle, {
        zoom: 15,
        center: scope.currentLocation,
        scrollwheel: false,
        styles: mapConfig.styles,
        panControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        fullScreenControl: false
      });
    }).catch((error) => {
      console.log(error);
    }).then(() => {
      this.calculateAndDisplayRoute(this.directionsService, this.directionsDisplay);
    })
  }

  /**
   * drow the route according to the bus driver using way points
   */
  calculateAndDisplayRoute(directionsService, directionsDisplay) {
    let scope = this;
    // var waypts = [{ location: new google.maps.LatLng(6.924532, 79.978428) },
    // { location: new google.maps.LatLng(6.903943, 79.955009) },
    // { location: new google.maps.LatLng(6.907623, 79.929085) },
    // { location: new google.maps.LatLng(6.902906, 79.908347) },
    // { location: new google.maps.LatLng(6.910562, 79.887805) },
    // { location: new google.maps.LatLng(6.902906, 79.908347) }];
    directionsService.route({
      origin: "Kaduwela",
      destination: "Kollupitiya",
      travelMode: 'DRIVING',
      // waypoints: waypts
    }, function (response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
        directionsDisplay.setOptions({ suppressMarkers: true });
        directionsDisplay.setMap(scope.map)
        scope.viewBuses();
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

  /**
   * get active tracker locations from the database and view on the map
   */
  viewBuses() {
    let scope = this;
    this.locations = this.locationSocket.getMessages().subscribe(data => {
      scope.deleteMarkers();
      console.log(data)
      var t = JSON.parse(JSON.stringify(data))
      t.data.forEach(element => {
        let updatelocation = new google.maps.LatLng(element.latitude, element.longitude);
        var image = "assets/img/busmarker.png"
        this.addMarker(updatelocation, image);
      })
      this.setMapOnAll(this.map);
    });
    this.sendMessage();
  }

  addMarker(location, image) {
    var icon = {
      url: image, // url
      scaledSize: new google.maps.Size(30, 30), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    };
    console.log(icon)
    console.log(location);
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      // animation: google.maps.Animation.BOUNCE,
      icon: icon
    });
    this.markers.push(marker);
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }

  setTimer(minutes) {
    //   for (let i = 3; i >= 0; i--) {
    //     setTimeout(function () {
    //       for (let r = 60; r > 0; r--) {
    //         this.time = "00:0" + i + ":" + r;
    //       }
    //     }, 1000);
    //   }
  }


}
