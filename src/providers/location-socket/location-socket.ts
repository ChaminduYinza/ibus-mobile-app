import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../../config/config'
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
/*
  Generated class for the LocationSocketProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationSocketProvider {
  private url = config.Socket_URL;
  private socket;

  constructor(public http: HttpClient) {
    console.log('Hello RealtimeLocationServiceProvider Provider');
  }

  sendMessage(message) {
    console.log(message)
    this.socket.emit('add-message', message);
  }

  getMessages() {
    console.log(this.url)
    console.log("getMessage")
    let observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('message', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }
}
