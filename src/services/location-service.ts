import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config/config';
// import {  Response , Http} from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the LocationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationProvider {
  constructor(public http: HttpClient) {
    console.log('Hello LocationProvider Provider');
  }
  sendLocation(location){
    return (this.http.post(config.api_url+"/cordinate/new",location).map((res) => res));
  }
  startTracking(location){
    return (this.http.post(config.api_url+"/tracking/new",location).map((res) => res));
  }
  updateTracking(location){
    return (this.http.post(config.api_url+"/tracking/update",location).map((res) => res))
  }
  createJourney(location){
    return (this.http.post(config.api_url+"/journey/create",location).map((res) => res));
  }
  endJourney(location){
    return (this.http.post(config.api_url+"/journey/end",location).map((res) => res));
  }
  getTrackers(object){
    return (this.http.post(config.api_url+"/tracking/route",object).map((res) => res));
  }
}
