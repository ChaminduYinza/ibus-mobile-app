import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config/config';
import 'rxjs/add/operator/map';

/*
  Generated class for the LocationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginService {
  constructor(public http: HttpClient) {
    console.log('Hello LocationProvider Provider');
  }
  registerUSer(user){
    return (this.http.post(config.api_url+"/user/new",user).map(res=>res));
  }
  loginService(credientials){
    return (this.http.post(config.api_url+"/user/login",credientials).map(res => res))
  }
}
